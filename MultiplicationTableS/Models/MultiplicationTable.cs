﻿using System;
using System.Collections.Generic;

namespace MultiplicationTableS.Models
{
    /// <summary>
    /// This class is used for defining an array for the multiplication matrix and 
    /// for computing the matrix componenets based on the size and base of the required 
    /// multiplication table.
    /// </summary>
    public class MultiplicationMatrix
    {
        //Define an array for the multiplication matrix.
        public IList<List<string>> matrixComponents;

        /// <summary>
        /// This constructor calls the createMatrix method to compute the components of the 
        /// multiplication table.
        /// </summary>
        /// <param name="table"> An object of the Table class with two variables.</param>
        public MultiplicationMatrix(Table table)
        {
            matrixComponents = new List<List<string>>();

            //Call the createMatrix method to compute the multiplication table.
            createMatrix(table.MatrixSize, table.MatrixBase);
        }

        /// <summary>
        /// This method computes the components of the multiplication table.
        /// </summary>
        /// <param name="size"> The size of the multiplication table.</param>
        /// <param name="mbase"> The base of the multiplication table.</param>
        /// <returns>
        /// This method returns a matrix of the computed components of the 
        /// multiplication table.
        /// </returns>
        public void createMatrix(int size, string mbase)
        {
            //To have the first row and column, the value of the size variable 
            //is increased by one. 
            size++;

            //Define a vector to save each row of the matrix.
            List<string> rowComponents = new List<string>();

            //Compute the first row of the multiplication table.
            rowComponents.Add("x");
            for (int j = 1; j < size; j++)
            {
                rowComponents.Add(intComponent(j, mbase));
            }
            matrixComponents.Add(rowComponents);

            //Compute the remaining rows of the multiplication table.
            for (int i = 1; i < size; i++)
            {
                //Define a vector to save each row of the matrix.
                rowComponents = new List<string>();

                //Compute each row of the multiplication table.
                rowComponents.Add(intComponent(i, mbase));
                for (int j = 1; j < size; j++)
                {
                    rowComponents.Add(intComponent(i * j, mbase));
                }

                //Save the computed row as a row of the matrix.
                matrixComponents.Add(rowComponents);
            }
        }

        /// <summary>
        /// This method converts a decimal number to binary or hexadecimal based on 
        /// the required type of the multiplication table and saves it as a returned 
        /// string.
        /// </summary>
        /// <param name="num"> A decimal number.</param>
        /// <param name="mbase"> The base of the multiplication table.</param>
        /// <returns>
        /// This method returns a string involving a decimal, binary or 
        /// hexadecimal number.
        /// </returns>
        public string intComponent(int num, string mbase)
        {
            string strComponent;

            //Check that which type of the number the method must return.
            if ((mbase.ToLower()).Equals("binary"))
            {
                //Convert the decimal number to a binary number and save it as a string.
                strComponent = Convert.ToString(num, 2);

                return strComponent;
            }
            else if ((mbase.ToLower()).Equals("hex"))
            {
                //Convert the decimal number to a hexadecimal number and save it as a string.
                strComponent = Convert.ToString(num, 16);

                return strComponent;
            }

            return num.ToString();
        }
    }
}