﻿using System.ComponentModel.DataAnnotations;
using System.IO;

namespace MultiplicationTableS.Models
{
    /// <summary>
    /// This class is used for defining the Table model involving the size and 
    /// base of the required multiplication table.
    /// </summary>
    public class Table
    {
        //The size of the multiplication table.
        private int matrixSize;

        //The base of the multiplication table.
        private string matrixBase;

        /// <summary>
        /// This constructor is used for instantiate an object without setting the size and 
        /// base of the required multiplication table.
        /// </summary>
        public Table()
        {
        }

        /// <summary>
        /// This constructor is used for instantiate an object by setting the size and 
        /// base of the required multiplication table.
        /// </summary>
        /// <param name="matrixsize"> The size of the multiplication table.</param>
        /// <param name="matrixBase"> The base of the multiplication table.</param>
        /// <returns>
        /// This constructor returns the parameters of the instantiated object.
        /// </returns>
        public Table(int matrixSize, string matrixBase)
        {
            this.MatrixSize = matrixSize;
            this.MatrixBase = matrixBase;
        }

        /// <summary>
        /// This method is the setter and getter of the instantiated value of the size 
        /// of the required multiplication table.
        /// </summary>
        [Required(ErrorMessage = "The size of multiplication table is required.")]
        [Display(Name = "Matrix Size:")]
        public int MatrixSize
        {
            get => matrixSize;
            set
            {
                if (value < 3 || value > 15)
                {
                    throw new InvalidDataException("The value of matrix size must be between 3 to 15.");
                }

                matrixSize = value;
            }
        }

        /// <summary>
        /// This method is the setter and getter of the instantiated value of the base 
        /// of the required multiplication table.
        /// </summary>
        [Required(ErrorMessage = "The base of multiplication table is required.")]
        [Display(Name = "Matrix Base:")]
        public string MatrixBase
        {
            get => matrixBase;
            set
            {
                if (!((value.ToLower()).Equals("decimal")) || !((value.ToLower()).Equals("binary"))
                    || !((value.ToLower()).Equals("hex")))
                {
                    matrixBase = value;
                }
                else
                {
                    throw new InvalidDataException("The type of multiplication table is not correct.");
                }
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}