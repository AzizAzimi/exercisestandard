﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MultiplicationTableS.Models;

namespace MultiplicationTableS.Controllers
{
    /// <summary>
    /// This controller is one of the program controllers that it works on everything for 
    /// designing the multiplication table involving page designing, table designing and 
    /// manipulating, inputs and outputs controling and the related pages views.
    /// </summary>
    public class TableController : Controller
    {
        /// <summary>
        /// This action is the main action of Table controller.
        /// It shows the initiliezed page of the Table controller.
        /// </summary>
        /// <remarks>
        /// This action is a Get type. It is called by clicking on the Multiplication-Table tab.
        /// </remarks>
        /// <returns>
        /// This action returns an object of the Table class and a matrix of the computed 
        /// components of the multiplication table.
        /// </returns>
        [HttpGet]
        public ActionResult Index()
        {
            //Use try-catch block to check of throwing an exception because of an error.
            try
            {
                //Instantiate an object of the Table class using the initilized variables.
                Table table = new Table(10, "decimal");

                //Compute the multiplication table for the first time.
                MultiplicationMatrix multiplicationMatrix = new MultiplicationMatrix(table);

                //Save some variables for transfering them to the view.
                ViewData["table"] = table;
                ViewData["matrix"] = multiplicationMatrix.matrixComponents;
            }
            catch (InvalidDataException ex)
            {
                //Make a message and a javascript alert function when we have a thrown exception.
                //This function will show an error message to user on the client side.
                string message = "<script>alert('Error in input data: " + ex.Message
                    + "');</script>";

                //Write the javascript function in the client side (Fron-End) of the program.
                Response.Write(message);
            }
            return View();
        }

        /// <summary>
        /// This action works on getting some variables from the view, instantiating an 
        /// object of the Table class, computing the multiplication table and transfering some 
        /// variables to the view, and also shows the multiplication-table page.
        /// </summary>
        /// <remarks>
        /// This action is a Post type  and is called by submitting the form in the view page.
        /// </remarks>
        /// <param name="tableIn"> An object of the Table class with two variables.</param>
        /// <returns>
        /// This action returns an object of the Table class and a matrix of the computed 
        /// components of the multiplication table.
        /// </returns>
        [HttpPost]
        public ActionResult Index(Table tableIn)
        {
            //Use try-catch block to check of throwing an exception because of an error.
            try
            {
                //Instantiate an object of the Table class using the variables coming from the view.
                Table table = new Table(tableIn.MatrixSize, tableIn.MatrixBase);

                //Compute the multiplication table using the variables coming from the view.
                MultiplicationMatrix multiplicationMatrix = new MultiplicationMatrix(table);

                //Save some variables for transfering them to the view.
                ViewData["table"] = table;
                ViewData["matrix"] = multiplicationMatrix.matrixComponents;
            }
            catch (InvalidDataException ex)
            {
                //Make a message and a javascript alert function when we have a thrown exception.
                //This function will show an error message to user on the client side.
                string message = "<script>alert('Error in input data: " + ex.Message
                    + "');</script>";

                //Write the javascript function in the client side (Fron-End) of the program.
                Response.Write(message);
            }
            return View();
        }

        /// <summary>
        /// This action works on getting some variables from the view, instantiating an 
        /// object of the Table class, compute the multiplication table and transfering some 
        /// variables to the view, and also show the multiplication-table page.
        /// </summary>
        /// <remarks>
        /// This action is a Get type and is called by selecting the multiplication-table 
        /// in the view page.
        /// </remarks>
        /// <param name="size"> The size of the multiplication table.</param>
        /// <param name="mbase"> The base of the multiplication table.</param>
        /// <returns>
        /// This action returns an object of the Table class and a matrix of the computed 
        /// components of the multiplication table.
        /// </returns>
        [HttpGet]
        public ActionResult Index1(int size, string mbase)
        {
            //Use try-catch block to check of throwing an exception because of an error.
            try
            {
                //Instantiate an object of the Table class using the variables coming from the view.
                Table table = new Table(size, mbase);

                //Compute the multiplication table using the variables coming from the view.
                MultiplicationMatrix multiplicationMatrix = new MultiplicationMatrix(table);

                //Save some variables for transfering them to the view.
                ViewData["table"] = table;
                ViewData["matrix"] = multiplicationMatrix.matrixComponents;
            }
            catch (InvalidDataException ex)
            {
                //Make a message and a javascript alert function when we have a thrown exception.
                //This function will show an error message to user on the client side.
                string message = "<script>alert('Error in input data: " + ex.Message
                    + "');</script>";

                //Write the javascript function in the client side (Fron-End) of the program.
                Response.Write(message);
            }
            return View("~/Views/Table/Index.cshtml");
        }
    }
}