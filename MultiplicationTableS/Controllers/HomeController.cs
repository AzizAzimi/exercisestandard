﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MultiplicationTableS.Controllers
{
    /// <summary>
    /// This controller is the main controller of the program.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// This action is the main action of the Home controller.
        /// It shows the first page of the program web.
        /// </summary>
        /// <remarks>
        /// This action is a Get type. It is called by clicking on the Home tab.
        /// </remarks>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This action gives some information about the program.
        /// </summary>
        /// <remarks>
        /// This action is a Get type. It is called by clicking on the About tab.
        /// </remarks>
        public ActionResult About()
        {
            ViewBag.Message = "X company is known the best company in Canada for year 2018.";

            return View();
        }

        /// <summary>
        /// This action gives some information about the program's owner.
        /// </summary>
        /// <remarks>
        /// This action is a Get type. It is called by clicking on the Contact tab.
        /// </remarks>
        public ActionResult Contact()
        {
            ViewBag.Message = "We have two headquarters in Canada, Montreal and Ottawa.";

            return View();
        }
    }
}